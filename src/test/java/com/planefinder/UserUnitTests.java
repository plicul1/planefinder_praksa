package com.planefinder;

import com.planefinder.User.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserUnitTests {
    @Autowired
    UserController userController;
    @Autowired
    private UserRepository repository;
    @Autowired
    private UserService service;

    @Test
    public void register_returnsDTO(){
        NewUser testUser = new NewUser("abcd");
        assertThat(userController.register(testUser)).isEqualTo(new UserDTO(testUser, true));
    }

    @Test
    public void duplicateRegister(){
        NewUser testUser = new NewUser("abcd");
        assertThat(userController.register(testUser)).isEqualTo(new UserDTO( false));
    }

}
