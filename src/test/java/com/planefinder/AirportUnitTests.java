package com.planefinder;

import com.planefinder.Airport.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AirportUnitTests {

    @Autowired
    AirportController airportController;
    @Autowired
    private AirportRepository repository;
    @Autowired
    private AirportService service;

    @Test
    public void getAirportByCode_returnsDTO(){
        Airport testAirport = new Airport();
        testAirport.setCode("ABCD");
        testAirport.setIcao("123");
        repository.save(testAirport);

        assertThat(airportController.getAirportByIcao(testAirport).equals(new AirportDTO(testAirport,true))).isTrue();
        //assertThat(airportController.getAirportByCode(testAirport)).isEqualTo(new AirportDTO(testAirport,true));
    }

}