package com.planefinder.Health;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @GetMapping(value = "/api/health")
    public Health a(){
        return new Health(true);
    }
}
