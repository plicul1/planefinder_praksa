package com.planefinder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.planefinder.Airport.Airport;
import com.planefinder.Airport.AirportRepository;
import com.planefinder.User.NewUser;
import com.planefinder.User.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

@SuppressWarnings("ConfigurationProperties")
@Configuration
@ConfigurationProperties
public class LoadDatabase {
    private String airportsFilepath;
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    public String getairportsFilepath() {
        return airportsFilepath;
    }

    public void setairportsFilepath(String airports) {
        this.airportsFilepath = airports;
    }

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, AirportRepository airportRepository) {

        Type REVIEW_TYPE = new TypeToken<List<Airport>>() {
        }.getType();
        Gson gson = new Gson();
        JsonReader reader= null;
        try {
            //reader = new JsonReader(new FileReader("src/main/java/com/example/demo/airports.json"));
           reader = new JsonReader(new FileReader(airportsFilepath));
        } catch (FileNotFoundException e) {}
        List<Airport> data = gson.fromJson(reader, REVIEW_TYPE);
        airportRepository.saveAll(data);

        return args -> {
            log.info("Preloading " + userRepository.save(new NewUser("psimec")));
        };
    }

}
