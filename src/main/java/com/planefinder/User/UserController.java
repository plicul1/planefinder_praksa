package com.planefinder.User;

//import com.example.demo.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService service;
    //private Repository repository;

    @PostMapping(value = "api/register", consumes = "application/json")
    public UserDTO register(@RequestBody NewUser newUser) {
        //if (repository.saveUser(newUser)) return new UserDTO(newUser, true);
        if (service.saveUser(newUser)) return new UserDTO(newUser, true);
        return new UserDTO(false);
    }


}