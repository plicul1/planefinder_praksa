package com.planefinder.User;

//import com.example.demo.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class CustomIdentityAuthenticationProvider
        implements AuthenticationProvider {
    @Autowired
    //private Repository repository;
    private UserService service;


    UserDetails isValidUser(String username, String password) {
        NewUser temp;
        temp = service.findUser(username);
        if(temp==null) return null;
        if (username.equals(temp.getUsername())
                && password.equals(temp.getPassword())) {
            UserDetails user = User
                    .withUsername(username)
                    .password(password)
                    .roles("USER_ROLE")
                    .build();
            return user;
        }
        return null;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        UserDetails userDetails = isValidUser(username, password);

        if (userDetails != null) {
            return new UsernamePasswordAuthenticationToken(
                    username,
                    password,
                    userDetails.getAuthorities());
        } else {
            throw new BadCredentialsException("Wrong credentials");
        }
    }

    @Override
    public boolean supports(Class<?> authenticationType) {
        return authenticationType
                .equals(UsernamePasswordAuthenticationToken.class);
    }
}
