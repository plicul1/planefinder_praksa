package com.planefinder.User;

//import com.example.demo.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService   {
    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        NewUser temp;
        temp = findUser(username);
        if(temp!=null){
            return User.withUsername(temp.getUsername()).password(temp.getPassword()).authorities("USER").build();
        }
        else {
            throw new UsernameNotFoundException(String.format("Username[%s] not found", username));
        }
    }

    public boolean userExists(String Username){
        return repository.existsNewUserByUsername(Username);
    }
    public boolean saveUser(NewUser user){
        if(this.userExists(user.getUsername())) return false;
        return repository.save(user)==user;
    }
    public NewUser findUser(String username){
        try{
            return repository.findByUsername(username).get(0);
        }
        catch (IndexOutOfBoundsException e){
            return null;
        }
    }
}
