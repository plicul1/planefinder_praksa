package com.planefinder.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<NewUser,Long>{

    public boolean existsNewUserByUsername(String username);
    List<NewUser> findByUsername(String username);


    //@Query(value = "SELECT true FROM NewUser s WHERE s.username= :userName")
    //public boolean existsNew_UserByUsername(@Param("userName") String username);
}
