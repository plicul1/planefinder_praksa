package com.planefinder.User;


import com.planefinder.Response;

import java.util.Objects;

public class UserDTO {
    private String password ;
    private Response response;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(password, userDTO.password) &&
                Objects.equals(response, userDTO.response);
    }



    public UserDTO(NewUser user, boolean response) {
        this.password = user.getPassword();
        this.response = new Response(response,response ? "No error":"Username already exists!");
    }

    public UserDTO(boolean response) {
        this.password=null;
        this.response = new Response(response,response ? "No error":"Username already exists!");
    }

}
