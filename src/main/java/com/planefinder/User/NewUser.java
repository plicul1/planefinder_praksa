package com.planefinder.User;

import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.*;

@Entity
@Table
public class NewUser {
    private @Id @GeneratedValue Long id;
    @Column(unique=true)
    private String username;
    private String password = RandomStringUtils.randomAlphanumeric(10);

    @Override
    public String toString() {
        return this.username;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public NewUser(String username) {
        this.username = username;
        if(username.equals("psimec"))this.password = "abcdefg";

    }

    public NewUser(){
    }


}
