package com.planefinder;


import java.util.Objects;

public class Response {
    boolean success;
    String message;

    public Response(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return success == response.success &&
                Objects.equals(message, response.message);
    }


}
