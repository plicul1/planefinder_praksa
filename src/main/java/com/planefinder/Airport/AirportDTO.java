package com.planefinder.Airport;

import com.planefinder.Response;


public class AirportDTO {
    private Airport airport;

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    private Response response;
/*    @SerializedName(value="code")
    private String code;
    @SerializedName(value="lat")
    private String lat;
    @SerializedName(value="lon")
    private String  lon;
    @SerializedName(value="name")
    private String name;
    @SerializedName(value="city")
    private String city;
    @SerializedName(value="state")
    private String state;
    @SerializedName(value="country")
    private String country;
    @SerializedName(value="woeid")
    private String woeid;
    @SerializedName(value="tz")
    private String tz;
    @SerializedName(value="phone")
    private String phone;
    @SerializedName(value="type")
    private String type;
    @SerializedName(value="email")
    private String email;
    @SerializedName(value="url")
    private String url;
    @SerializedName(value="runway_length")
    private Double runway_length;
    @SerializedName(value="icao")
    private String icao;
    @SerializedName(value="direct_flights")
    private Integer direct_flights;
    @SerializedName(value="carriers")
    private Integer carriers;

    private Response response;

    public AirportDTO(Airport airport, boolean response) {
        this.code = airport.getCode();
        this.lat = airport.getLat();
        this.lon = airport.getLon();
        this.name = airport.getName();
        this.city = airport.getCity();
        this.state = airport.getState();
        this.country = airport.getCountry();
        this.woeid = airport.getWoeid();
        this.tz = airport.getTz();
        this.phone = airport.getPhone();
        this.type = airport.getType();
        this.email = airport.getEmail();
        this.url = airport.getUrl();
        this.runway_length = airport.getRunway_length();
        this.icao = airport.getIcao();
        this.direct_flights = airport.getDirect_flights();
        this.carriers = airport.getCarriers();
        this.response = new Response(response,response ? "No error":"Not found!");
    }
*/
    public AirportDTO(Airport airport, boolean response){
        this.airport=airport;
        this.response = new Response(response,response ? "No error":"Not found!");
    }
    public AirportDTO(boolean response) {
        this.response = new Response(response,response ? "No error":"Not found!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirportDTO that = (AirportDTO) o;
        return airport.getIcao().equals(that.airport.getIcao()) && response.equals(that.response);
    }

}
