package com.planefinder.Airport;

//import com.example.demo.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AirportController {
    @Autowired
    //private Repository repository;
    private AirportService service;

    @PostMapping(value = "/api/airport", consumes = "application/json")
    public AirportDTO getAirportByIcao(@RequestBody Airport icao){
        Airport temp=service.findAirport(icao.getIcao());
        if(temp==null) return new AirportDTO(false);
        return new AirportDTO(temp,temp != null);
    }

}
