package com.planefinder.Airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("AirportService")
public class AirportService {
    @Autowired
    private AirportRepository airportRepository;


    public Airport findAirport(String icao){
        try{
            return airportRepository.findByIcao(icao).get(0);
        }
        catch (IndexOutOfBoundsException e){
            return null;
        }
    }

}
