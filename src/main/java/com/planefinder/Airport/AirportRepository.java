package com.planefinder.Airport;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;




@Repository
@Transactional
public interface AirportRepository extends JpaRepository<Airport,Long> {
    List<Airport> findByCode(String code);
    List<Airport> findByIcao(String icao);

}
