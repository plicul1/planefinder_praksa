package com.planefinder.Airport;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class Airport {
    @SerializedName(value="code") @Id
    private String code;
    @SerializedName(value="lat")
    private String lat;
    @SerializedName(value="lon")
    private String  lon;
    @SerializedName(value="name")
    private String name;
    @SerializedName(value="city")
    private String city;
    @SerializedName(value="state")
    private String state;
    @SerializedName(value="country")
    private String country;
    @SerializedName(value="woeid")
    private String woeid;
    @SerializedName(value="tz")
    private String tz;
    @SerializedName(value="phone")
    private String phone;
    @SerializedName(value="type")
    private String type;
    @SerializedName(value="email")
    private String email;
    @SerializedName(value="url")
    private String url;
    @SerializedName(value="runway_length")
    private Double runway_length;
    @SerializedName(value="icao")
    private String icao;
    @SerializedName(value="direct_flights")
    private Integer direct_flights;
    @SerializedName(value="carriers")
    private Integer carriers;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWoeid() {
        return woeid;
    }

    public void setWoeid(String woeid) {
        this.woeid = woeid;
    }

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getRunway_length() {
        return runway_length;
    }

    public void setRunway_length(Double runway_length) {
        this.runway_length = runway_length;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Integer getDirect_flights() {
        return direct_flights;
    }

    public void setDirect_flights(Integer direct_flights) {
        this.direct_flights = direct_flights;
    }

    public Integer getCarriers() {
        return carriers;
    }

    public void setCarriers(Integer carriers) {
        this.carriers = carriers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airport airport = (Airport) o;
        return Objects.equals(icao, airport.icao);
    }

}
